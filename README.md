# README #


### What is this repository for? ###

* Sample regression suite for Sauce App as paer of Automation Assessment
* 1.0

### Pre Requisites ###

* To run this automation suite, you just need to have a machine(Mac/Windows) with chrome browser installed and compatible chromedriver downloaded.

### Tech Stack Used ###

* Automation Tool :  Selenium
* Programming Language : Java
* Frameworks : Page Object Model along with TestNG
* Reports : TestNG default reports
