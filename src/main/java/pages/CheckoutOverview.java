package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class CheckoutOverview {
    WebDriver driver;

    public CheckoutOverview(WebDriver driver) {
        this.driver = driver;
    }

    @FindBy(how = How.XPATH, using = "//button[@data-test='finish']")
    WebElement finishButton;

    public void clickOnFinish(){
        finishButton.click();
    }
}