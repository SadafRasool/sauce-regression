package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class Login {
    WebDriver driver;

    public Login(WebDriver driver) {
        this.driver = driver;
    }

    @FindBy(how= How.XPATH, using = "//input[@data-test='username']")
    WebElement userName;

    @FindBy(how= How.XPATH, using = "//input[@data-test='password']")
    WebElement password;

    @FindBy(how= How.XPATH, using = "//input[@data-test='login-button']")
    WebElement loginButton;

    public void enterUserName(String name){
        userName.sendKeys(name);
    }

    public void enterPassword(String pass){
        password.sendKeys(pass);
    }

    public void clickOnLoginButton(){
        loginButton.click();
    }
}