package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class Checkout {
    WebDriver driver;

    public Checkout(WebDriver driver) {
        this.driver = driver;
    }


    @FindBy(how = How.XPATH, using = "//input[@data-test='firstName']")
    WebElement firstName;


    @FindBy(how = How.XPATH, using = "//input[@data-test='lastName']")
    WebElement lastName;


    @FindBy(how = How.XPATH, using = "//input[@data-test='postalCode']")
    WebElement postalCode;

    @FindBy(how = How.XPATH, using = "//input[@data-test='continue']")
    WebElement continueButton;

    public void enterFirstName(String firstNameVal){
        firstName.sendKeys(firstNameVal);
    }

    public void enterLastName(String lastNameVal){
        lastName.sendKeys(lastNameVal);
    }

    public void enterPostalCode(String postalCodeVal){
        postalCode.sendKeys(postalCodeVal);
    }

    public void clickOnContinue(){
        continueButton.click();
    }
}