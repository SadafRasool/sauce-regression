package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class AddToCart {
    WebDriver driver;

    @FindBy(how= How.XPATH, using = "//a[@class='shopping_cart_link']")
    WebElement cartIcon;

    public AddToCart(WebDriver driver) {
        this.driver = driver;
    }

    public void clickOnCart(){
        cartIcon.click();
    }

    public void addProductToCart(String productId) {
        driver.findElement(By.xpath("//button[@data-test='add-to-cart-" +productId+"']")).click();
    }
}