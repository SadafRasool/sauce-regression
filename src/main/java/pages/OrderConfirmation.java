package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class OrderConfirmation {
    WebDriver driver;

    public OrderConfirmation(WebDriver driver) {
        this.driver = driver;
    }

    @FindBy(how = How.XPATH, using = "//h2[@class='complete-header']")
    WebElement confirmationMessage;

    public boolean validateConfirmationMessage(String expectedMessage){
        return confirmationMessage.getText().equalsIgnoreCase(expectedMessage);
    }
}