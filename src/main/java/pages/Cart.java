package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class Cart {
    WebDriver driver;

    public Cart(WebDriver driver) {
        this.driver = driver;
    }

    @FindBy(how= How.XPATH, using = "//button[@data-test='checkout']")
    WebElement checkoutButton;

    @FindBy(how= How.XPATH, using = "//div[@class='cart_quantity']")
    WebElement quantity;

    public void clickOnCheckoutButton(){
        checkoutButton.click();
    }

    public int getItemsCount(){
        return driver.findElements(By.xpath("//div[@class='cart_item']")).size();
    }

    public int getItemQuantity(){
        return Integer.parseInt(quantity.getText());
    }
}
