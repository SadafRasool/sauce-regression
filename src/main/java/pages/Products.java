package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Products {
    WebDriver driver;
    public Products(WebDriver driver) {
        this.driver = driver;
    }

    public void selectProduct(String productName) {
        driver.findElement(By.xpath("//img[@alt='" + productName + "']")).click();
    }
}