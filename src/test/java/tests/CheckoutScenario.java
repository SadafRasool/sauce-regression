package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import pages.*;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class CheckoutScenario {
    private WebDriver driver;
    private Properties prop;
    private String userName;
    private String password;
    private String productName;
    private String firstName;
    private String lastName;
    private String postalCode;
    private String successMessage;
    private String url;
    private String productId;
    private Login login;
    private Products product;
    private AddToCart addToCart;
    private Cart cart;
    private Checkout checkout;
    private CheckoutOverview checkoutOverview;
    private OrderConfirmation orderConfirmation;
    /**
     *
     * Test Name :  testSauceApp
     * Objective :  To Test E2E Checkout scenario for the given App
     * Pre-Requisites : Need to have valid test data in test_data.properties file under /resources directory
     * Result : Test NG Report will print the log message in case test is failed
     * **/

    @Test
    void testSauceApp() {
        /**
         *
         * Logging into App
         *
         * **/
        login.enterUserName(userName);
        login.enterPassword(password);
        login.clickOnLoginButton();

        /**
         *
         * Selecting the product based on product name given in test data file
         *
         * **/
        product.selectProduct(productName);

        /**
         *
         * Adding product to cart
         *
         * **/
        addToCart.addProductToCart(productId);

        /**
         *
         * Navigating to Cart Button
         *
         ***/
        addToCart.clickOnCart();


        /**
         *
         * Validating only one item is shown in cart
         *
         ***/
        if (cart.getItemsCount() != 1){
            Reporter.log("Scenario failed, as there are more than 1 items shown in cart");
        }
        Assert.assertEquals(cart.getItemsCount(), 1);

        /**
         *
         * Validating only selected quantity for the selected item is shown in cart
         *
         ***/
        if (cart.getItemQuantity() != 1){
            Reporter.log("Scenario failed, as there is more than 1 quantity shown in cart");
        }
        Assert.assertEquals(cart.getItemQuantity(), 1);

        /**
         *
         * Navigating to check out
         *
         ***/
        cart.clickOnCheckoutButton();


        /**
         *
         * filling out check out form
         *
         ***/
        checkout.enterFirstName(firstName);
        checkout.enterLastName(lastName);
        checkout.enterPostalCode(postalCode);
        checkout.clickOnContinue();

        /**
         *
         * filling out check out form
         *
         ***/
        checkoutOverview.clickOnFinish();

        /**
         *
         * Verifying success Message
         *
         ***/
        boolean result = orderConfirmation.validateConfirmationMessage(successMessage);

        if (result){
            Reporter.log("Checkout scenario is passed.");
        }else{
            Reporter.log("Checkout scenario is failed.");
        }
        Assert.assertTrue(result, "Checkout scenario is failed.");
    }


    @BeforeSuite
    void beforeRun(){
        try (InputStream input = getClass().getClassLoader().getResourceAsStream("test_data.properties")) {

            /***
             *
             * Loading property file
             *
             * **/
            prop = new Properties();
            Assert.assertNotNull(input, "Error, seems like Test Data File is not found!");
            prop.load(input);

            /****
             *
             * Initializing variables for testing
             *
             * **/
            userName = prop.getProperty("userName");
            password = prop.getProperty("password");
            productName = prop.getProperty("productName");
            firstName = prop.getProperty("firstName");
            lastName = prop.getProperty("lastName");
            postalCode = prop.getProperty("postalCode");
            successMessage = prop.getProperty("successMessage");
            url = prop.getProperty("url");
            productId = productName.toLowerCase().replace(" ", "-");


            /**
             *
             * Launching App
             * **/
            //System.setProperty("webdriver.chrome.driver", "your-path-to-chrome driver")  //applicable only for windows
            driver = new ChromeDriver();
            driver.manage().window().maximize();
            driver.get(url);

            /****
             * Initializing Page Objects
             * */
            login = PageFactory.initElements(driver, Login.class);
            product = PageFactory.initElements(driver, Products.class);
            addToCart = PageFactory.initElements(driver, AddToCart.class);
            cart = PageFactory.initElements(driver, Cart.class);
            checkout = PageFactory.initElements(driver, Checkout.class);
            checkoutOverview = PageFactory.initElements(driver, CheckoutOverview.class);
            orderConfirmation = PageFactory.initElements(driver, OrderConfirmation.class);
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    /**
     *
     * Closing the browser and freeing memory by killing objects
     *
     ***/
    @AfterSuite
    void afterRun(){
        driver.quit();
        prop = null;
        driver = null;
        userName = null;
        password = null;
        productName = null;
        firstName = null;
        lastName = null;
        postalCode = null;
        successMessage = null;
        url = null;
        productId = null;
        login = null;
        product = null;
        addToCart = null;
        cart = null;
        checkout = null;
        checkoutOverview = null;
        orderConfirmation = null;
    }
}